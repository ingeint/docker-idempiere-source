# Docker iDempiere Source 8.1

## Environment Variables

| Variable | Default Value | Description |
| - | - | - |
| IDEMPIERE_REPOSITORY | /idempiere | Path to iDempiere |
| IDEMPIERE_VERSION | 8.1 | iDempiere Version |

## Make Commands

`make build` creates idempiere-source docker image (with labels `idempiere-source:8.1` and `idempiere-source:latest`)

`make bash` creates a terminal within idempiere-source docker image

## Using from INGEINT Registry

Image: `portus.ingeint.com/idempiere-source`.

```bash
$ docker pull portus.ingeint.com/idempiere-source:8.1
```
